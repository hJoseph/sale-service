package com.fullstack.sales.service.salesservice.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Entity
@Table(name = "detail_table")
@ApiModel(description = "All  detail  about detail")
public class Detail {

    @Id
    @ApiModelProperty(notes = "the database generated id ID")
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private  Long id;


    @ApiModelProperty(notes = "the  lastPurchase")
    @Column(name = "lastPurchase", length = 100, nullable = false)
    private Integer  totalProducts;

    @ApiModelProperty(notes = "the  lastPurchase")
    @Column(name = "totalPrice", length = 100, nullable = false)
    private  Long totalPrice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
}
