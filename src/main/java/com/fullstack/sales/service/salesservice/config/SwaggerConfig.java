package com.fullstack.sales.service.salesservice.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("users-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.fullstack.sales.service.salesservice.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);



    }


    private ApiInfo apiEndPointsInfo () {
        return new ApiInfoBuilder()
                .title("User service Service API")
                .description("User Management REST API ")
                .contact(new Contact("Henry", "www.henry.com", "hjcalani@gmail.com"))
                .version("0.0.1")
                .license("Apache 1.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }


}
