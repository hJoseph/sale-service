package com.fullstack.sales.service.salesservice.input;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class DetailInput {

    private Long id;
    private  Integer totalProducts;
    private  Long totalPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
}
