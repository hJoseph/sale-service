package com.fullstack.sales.service.salesservice.controller;

import com.fullstack.sales.service.salesservice.input.ClientInput;
import com.fullstack.sales.service.salesservice.input.SaleInput;
import com.fullstack.sales.service.salesservice.model.domain.Client;
import com.fullstack.sales.service.salesservice.model.domain.Sale;
import com.fullstack.sales.service.salesservice.service.SaleCreateService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Sale rest",
        description = "Operations over Sale"
)
@RestController
@RequestMapping("/public/Sale")
@RequestScope
public class SaleController {

    @Autowired
    private SaleCreateService saleCreateService;

    @ApiOperation(
            value = "End point to create Sale"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code = 401,
                            message = "Unauthorized to create accout"
                    ),
                    @ApiResponse(
                            code = 404,
                            message = "Not found"
                    )
            }
    )

    @RequestMapping (value = "/create", method = RequestMethod.POST)
    public Sale createSale(@RequestBody SaleInput input){
        saleCreateService.setSaleInput(input);
        return  saleCreateService.save();

    }
}
