package com.fullstack.sales.service.salesservice.model.repositories;

import com.fullstack.sales.service.salesservice.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
