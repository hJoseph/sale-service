package com.fullstack.sales.service.salesservice.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Entity
@Table(name = "employee_table")

        @PrimaryKeyJoinColumns({
                @PrimaryKeyJoinColumn(name = "employeeid", referencedColumnName = "personid")
        })

@ApiModel(description = "All  detail  about sales")

public class Employee extends Person{

    @ApiModelProperty(notes = "the employee position")
    @Column(name = "position", length = 100, nullable = false)
    private  String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}

