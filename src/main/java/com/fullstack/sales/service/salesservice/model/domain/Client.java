package com.fullstack.sales.service.salesservice.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Entity
@Table(name = "client_table")

        @PrimaryKeyJoinColumns({
                @PrimaryKeyJoinColumn(name = "clientId", referencedColumnName = "personid")
        })
@ApiModel(description = "All  detail  about lastPurchase")
public class Client extends Person{

    @ApiModelProperty(notes = "the  lastPurchase")
    @Column(name = "lastPurchase", length = 100, nullable = false)
    private Date lastPurchase;

    public Date getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(Date lastPurchase) {
        this.lastPurchase = lastPurchase;
    }
}
