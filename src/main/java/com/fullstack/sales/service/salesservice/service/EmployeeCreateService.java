package com.fullstack.sales.service.salesservice.service;

import com.fullstack.sales.service.salesservice.input.EmployeeInput;
import com.fullstack.sales.service.salesservice.model.domain.Employee;
import com.fullstack.sales.service.salesservice.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class EmployeeCreateService {

    private EmployeeInput employeeInput;

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee save(){
        return employeeRepository.save(composeEmployeeInstance());
    }

    public Employee composeEmployeeInstance(){
        Employee instance = new Employee();
        instance.setPosition(employeeInput.getPosition());
        return  instance;
    }

    public void setEmployeeInput(EmployeeInput employeeInput) {
        this.employeeInput = employeeInput;
    }
}
