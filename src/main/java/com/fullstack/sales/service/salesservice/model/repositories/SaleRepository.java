package com.fullstack.sales.service.salesservice.model.repositories;

import com.fullstack.sales.service.salesservice.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface SaleRepository extends JpaRepository<Sale,Long> {
}
