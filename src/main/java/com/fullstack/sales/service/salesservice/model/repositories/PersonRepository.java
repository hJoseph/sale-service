package com.fullstack.sales.service.salesservice.model.repositories;

import com.fullstack.sales.service.salesservice.model.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface PersonRepository extends JpaRepository<Person,Long> {
}
