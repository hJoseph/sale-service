package com.fullstack.sales.service.salesservice.service;

import com.fullstack.sales.service.salesservice.input.SaleInput;
import com.fullstack.sales.service.salesservice.model.domain.Sale;
import com.fullstack.sales.service.salesservice.model.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class SaleCreateService {

     private SaleInput saleInput;

     @Autowired
     private SaleRepository saleRepository;

     public Sale save(){
         return saleRepository.save(composeSaleInstance());
     }

     public Sale composeSaleInstance(){
         Sale instance = new Sale();
         instance.setCreatedDate(saleInput.getCreatedDate());
         instance.setNumberSale(saleInput.getNumberSale());
         return  instance;
     }

    public void setSaleInput(SaleInput saleInput) {
        this.saleInput = saleInput;
    }
}
