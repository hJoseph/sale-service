package com.fullstack.sales.service.salesservice.input;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class ClientInput {

    private Date lastPurchase;

    public Date getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(Date lastPurchase) {
        this.lastPurchase = lastPurchase;
    }
}
