package com.fullstack.sales.service.salesservice.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Entity
@Table(name = "sale_table")
@ApiModel(description = "All  detail  about sales")
public class Sale {

    @Id
    @ApiModelProperty(notes = "the database generated  ID")
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ApiModelProperty(notes = "the account numberSale")
    @Column(name = "numberSale", length = 100, nullable = false)
    private Long numberSale;

    @ApiModelProperty(notes = "the account createdDate")
    @Column(name = "createdDate", nullable = false)
    private Date createdDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
