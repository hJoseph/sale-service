package com.fullstack.sales.service.salesservice.service;

import com.fullstack.sales.service.salesservice.input.ClientInput;
import com.fullstack.sales.service.salesservice.model.domain.Client;
import com.fullstack.sales.service.salesservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class ClientCreateService {
    private ClientInput clientInput;

    @Autowired
    private ClientRepository clientRepository;

    public Client save() {
        return clientRepository.save(composeClientInstance());
    }

    public Client composeClientInstance() {

        Client instance = new Client();
        instance.setLastPurchase(clientInput.getLastPurchase());
        return  instance;
    }

    public void setClientInput(ClientInput clientInput) {
        this.clientInput = clientInput;

    }

}
