package com.fullstack.sales.service.salesservice.service;

import com.fullstack.sales.service.salesservice.input.DetailInput;
import com.fullstack.sales.service.salesservice.model.domain.Detail;
import com.fullstack.sales.service.salesservice.model.repositories.DetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class DetailCreateService {
    private DetailInput detailInput;

    @Autowired
    private DetailRepository detailRepository;

    public Detail save() {
        return detailRepository.save(composeDetailInstance());
    }

    public Detail composeDetailInstance() {
        Detail instance = new Detail();
        instance.setTotalPrice(detailInput.getTotalPrice());
        instance.setTotalProducts(detailInput.getTotalProducts());
        return instance;
    }

    public void setDetailInput(DetailInput detailInput) {
        this.detailInput = detailInput;
    }

}
