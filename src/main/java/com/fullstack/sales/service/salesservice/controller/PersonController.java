package com.fullstack.sales.service.salesservice.controller;

import com.fullstack.sales.service.salesservice.input.DetailInput;
import com.fullstack.sales.service.salesservice.input.PersonInput;
import com.fullstack.sales.service.salesservice.model.domain.Detail;
import com.fullstack.sales.service.salesservice.model.domain.Person;
import com.fullstack.sales.service.salesservice.service.PersonCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Person rest",
        description = "Operations over Person"
)
@RestController
@RequestMapping("/public/Person")
@RequestScope
public class PersonController {

    @Autowired
    private PersonCreateService personCreateService;

  /*  @ApiOperation(
            value = "End point to create account"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code = 401,
                            message = "Unauthorized to create accout"
                    ),
                    @ApiResponse(
                            code = 404,
                            message = "Not found"
                    )
            }
    )

    @RequestMapping (value = "/createPerson", method = RequestMethod.POST)
    public Person createAccount(@RequestBody PersonInput input){
        personCreateService.sse
        return  detailCreateService.save();
    }
    */
}
