package com.fullstack.sales.service.salesservice.controller;

import com.fullstack.sales.service.salesservice.input.ClientInput;
import com.fullstack.sales.service.salesservice.input.DetailInput;
import com.fullstack.sales.service.salesservice.model.domain.Client;
import com.fullstack.sales.service.salesservice.model.domain.Detail;
import com.fullstack.sales.service.salesservice.service.DetailCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Detail rest",
        description = "Operations over Client"
)
@RestController
@RequestMapping("/Public/Detail")
@RequestScope
public class DetailController {

    @Autowired
    private DetailCreateService detailCreateService;

    @ApiOperation(
            value = "End point to create Detail"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code = 401,
                            message = "Unauthorized to create accout"
                    ),
                    @ApiResponse(
                            code = 404,
                            message = "Not found"
                    )
            }
    )


    @RequestMapping (value = "/create", method = RequestMethod.POST)
    public Detail createDetail(@RequestBody DetailInput input){
        detailCreateService.setDetailInput(input);
        return  detailCreateService.save();
    }


}
