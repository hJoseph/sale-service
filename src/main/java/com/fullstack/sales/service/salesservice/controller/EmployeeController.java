package com.fullstack.sales.service.salesservice.controller;

import com.fullstack.sales.service.salesservice.input.ClientInput;
import com.fullstack.sales.service.salesservice.input.EmployeeInput;
import com.fullstack.sales.service.salesservice.model.domain.Client;
import com.fullstack.sales.service.salesservice.model.domain.Employee;
import com.fullstack.sales.service.salesservice.service.EmployeeCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Employee rest",
        description = "Operations over Employee"
)
@RestController
@RequestMapping("/Public/Employee")
@RequestScope
public class EmployeeController {
    private EmployeeCreateService employeeCreateService;

    @ApiOperation(
            value = "End point to create employee"
    )
    @ApiResponses(
            {
                    @ApiResponse(
                            code = 401,
                            message = "Unauthorized to create accout"
                    ),
                    @ApiResponse(
                            code = 404,
                            message = "Not found"
                    )
            }
    )

    @RequestMapping (value = "/create", method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeInput input){
        employeeCreateService.setEmployeeInput(input);
        return  employeeCreateService.save();

    }
}
