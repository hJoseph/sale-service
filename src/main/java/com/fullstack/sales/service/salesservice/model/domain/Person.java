package com.fullstack.sales.service.salesservice.model.domain;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Entity
@Table(name = "person_table")

@Inheritance(strategy = InheritanceType.JOINED)
 public abstract class Person {

    @Id
    @Column(name = "personid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ApiModelProperty(notes = "the  email")
    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @ApiModelProperty(notes = "the  firstName")
    @Column(name = "firstName", length = 100, nullable = false)
    private String firstName;

    @ApiModelProperty(notes = "the  lastName")
    @Column(name = "lastName", length = 100, nullable = false)
    private String lastName;

    @ApiModelProperty(notes = "the  isDeleted")
    @Column(name = "isDeleted", length = 100, nullable = false)
    private Boolean isDeleted;

    @ApiModelProperty(notes = "the  lastPurchase")
    @Column(name = "lastPurchase", length = 100, nullable = false)
    private Date createdDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
