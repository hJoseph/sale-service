package com.fullstack.sales.service.salesservice.service;

import com.fullstack.sales.service.salesservice.input.PersonInput;
import com.fullstack.sales.service.salesservice.model.domain.Person;
import com.fullstack.sales.service.salesservice.model.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class PersonCreateService {

    private PersonInput personInput;

    @Autowired
    private PersonRepository personRepository;

  /*  public Person save(){
        return personRepository.save(composePersonInstance());
    }

    public Person composePersonInstance(){
        Person instance = new Person();

    }*/
}
