package com.fullstack.sales.service.salesservice.model.repositories;

import com.fullstack.sales.service.salesservice.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface ClientRepository extends JpaRepository<Client,Long> {
}
