package com.fullstack.sales.service.salesservice.input;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class SaleInput {
    private Long id;
    private  Long numberSale;
    private Date createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
