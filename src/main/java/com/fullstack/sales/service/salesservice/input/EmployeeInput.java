package com.fullstack.sales.service.salesservice.input;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class EmployeeInput {

    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
